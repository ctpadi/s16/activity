// activity
let num = parseInt(prompt("Enter number: "));
console.log("The number you provided is "+num+".");
for(let i = num; i>0; i--){
	if (i<=50) {
		console.log("The current value is at "+i+". Terminating the loop.")
		break;
	}
	else if (i%10 === 0) console.log("The number is divisible by 10. Skipping the number.");
	else if (i%5 === 0) console.log(i);
}

// stretch goals
let word = "supercalifragilisticexpialidocious";
let consonants = "";
for(let i = 0; i<word.length; i++)
{
	if("aeiou".includes(word[i].toLowerCase())) continue;
	else consonants += word[i];
}
console.log(consonants);